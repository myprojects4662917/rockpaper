package org.example;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static String computersTern(String[] variants) {
        Random random = new Random();
        int answer = random.nextInt(0, 3);
        return variants[answer];
    }
    public static void findWinner(String computer, String my) {
        if ((computer.equals("Ножницы") && my.equals("Бумага"))
                || (computer.equals("Бумага") && my.equals("Камень0"))
                || (computer.equals("Камень") && my.equals("Ножницы"))) {
            System.out.println("Победил компьютер!");
        } else if (computer.equals(my)) {
            System.out.println("Ничья!");
        } else {
            System.out.println("Мы выйграли!");
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] variants = {"Бумага", "Камень", "Ножницы"};

        String computerChoice = computersTern(variants);

        System.out.println("Введите ваше значение:");
        System.out.println("0 Бумага;");
        System.out.println("1 Камень;");
        System.out.println("2 Ножницы.");
        int ourTurn = scanner.nextInt();

        while (ourTurn > 2 || ourTurn < 0) {
            System.out.println("Вы ввели неправильное значение! Повторите попытку!");
            ourTurn = scanner.nextInt();
        }

        String ourChoice = variants[ourTurn];

        findWinner(computerChoice, ourChoice);

        System.out.println("Выбор копмрьютера: " + computerChoice);
        System.out.println("Выбор человека: " + ourChoice);
    }

}